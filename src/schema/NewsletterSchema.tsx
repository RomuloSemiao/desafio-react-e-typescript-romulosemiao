import * as Yup from "yup";

export default Yup.object().shape({
    newsletter: Yup.string()
        .min(6, "Precisa ter no mínimo 6 caracteres")
        .email("Email inválido"),
});
