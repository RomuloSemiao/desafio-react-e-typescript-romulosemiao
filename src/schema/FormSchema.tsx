import * as Yup from "yup";

const instaRegExp =
    /^(?:@)([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:(?!))){0,28}(?:[A-Za-z0-9_]))?)*$/;

export default Yup.object().shape({
    name: Yup.string()
        .min(4, "Precisa ter no mínimo 4 caracteres")
        .required("Campo obrigatório"),
    email: Yup.string()
        .min(6, "Precisa ter no mínimo 6 caracteres")
        .email("Email inválido")
        .required("Campo obrigatório"),
    cpf: Yup.string()
        .max(14, "Precisa ter até 11 dígitos")
        // .matches(cpfRegExp, "CPF inválido")
        .required("Campo obrigatório"),
    birthday: Yup.date()
        .max(new Date(), "Você precisa selecionar uma data antes de hoje")
        .required("Campo obrigatório"),
    phone: Yup.string()
        .max(15, "Precisa ter até 11 dígitos")
        .required("Campo obrigatório"),
    instagram: Yup.string()
        .min(2, "Precisa ter 2 dígitos")
        .matches(instaRegExp, "Perfil inválido, necessário o @")
        .required("Campo obrigatório"),
    validate: Yup.boolean().oneOf([true], "Você precisa aceitar os termos"),
});
