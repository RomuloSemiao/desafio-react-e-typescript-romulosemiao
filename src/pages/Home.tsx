import React from "react";

import "./Home.module.css";

import { Header } from "../components/Header/Header";
import { Breadcrumb } from "../components/Breadcrumbs/Breadcrumb";
import { Institutional } from "../components/Institutional/Institutional";
import { Newsletter } from "../components/Newsletter/Newsletter";
import { FooterLinks } from "../components/FooterLinks/FooterLinks";
import { Footer } from "../components/Footer/Footer";
import { ButtonScroll } from "../components/ButtonScroll/ButtonScroll";

import { MenuProvider } from "../contexts/MenuContext";
import { ButtonWpp } from "../components/ButtonWpp/ButtonWpp";

export function Home() {
    return (
        <MenuProvider>
            <Header />
            <Breadcrumb />
            <Institutional />
            <Newsletter />
            <FooterLinks />
            <Footer />
            <ButtonWpp />
            <ButtonScroll />
        </MenuProvider>
    );
}
