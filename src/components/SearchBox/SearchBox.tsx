import React from "react";

import { IconSearch } from "../../assets/images/IconSearch";

import styles from "./SearchBox.module.css";

export function SearchBox(props: any) {
    return (
        <div {...props}>
            <div className={styles["search-box"]}>
                <input
                    type="text"
                    className={styles["search-box-input"]}
                    placeholder="Buscar..."
                />
                <IconSearch className={styles["search-box-icon"]} />
            </div>
        </div>
    );
}
