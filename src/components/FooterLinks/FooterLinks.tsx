import React from "react";

import { FacebookIcon } from "../../assets/images/FacebookIcon";
import { InstagramIcon } from "../../assets/images/InstagramIcon";
import { LinkedinIcon } from "../../assets/images/LinkedinIcon";
import { TwitterIcon } from "../../assets/images/TwitterIcon";
import { YoutubeIcon } from "../../assets/images/YoutubeIcon";

import styles from "./FooterLinks.module.css";

export function FooterLinks() {
    return (
        <section className={styles["footer-links__wrapper"]}>
            <div className={styles["footer-links__mobile"]}>
                <details className={styles["footer-links__box"]}>
                    <summary className={styles["footer-links__title"]}>
                        INSTITUCIONAL
                    </summary>
                    <ul className={styles["footer-links__list"]}>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Quem Somos</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Política de Privacidade</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Segurança</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Seja um Revendedor</a>
                        </li>
                    </ul>
                </details>
                <details className={styles["footer-links__box"]}>
                    <summary className={styles["footer-links__title"]}>
                        DÚVIDAS
                    </summary>
                    <ul className={styles["footer-links__list"]}>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Entregas</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Pagamento</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Trocas e Devoluções</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Dúvidas Frequentes</a>
                        </li>
                    </ul>
                </details>
                <details className={styles["footer-links__box"]}>
                    <summary className={styles["footer-links__title"]}>
                        FALE CONOSCO
                    </summary>
                    <ul className={styles["footer-links__list"]}>
                        <b>Atendimento ao Consumidor</b>
                        <li className={styles["footer-links__item"]}>
                            <a href="tel:1141599504">(11) 4159-9504</a>
                        </li>
                        <b>Atendimento Online</b>
                        <li className={styles["footer-links__item"]}>
                            <a href="tel:11994338825">(11) 99433-8825</a>
                        </li>
                    </ul>
                </details>
            </div>
            <div className={styles["footer-links__desktop"]}>
                <div className={styles["footer-links__box"]}>
                    <h4 className={styles["footer-links__title"]}>
                        INSTITUCIONAL
                    </h4>
                    <ul className={styles["footer-links__list"]}>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Quem Somos</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Política de Privacidade</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Segurança</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Seja um Revendedor</a>
                        </li>
                    </ul>
                </div>
                <div className={styles["footer-links__box"]}>
                    <h4 className={styles["footer-links__title"]}>DÚVIDAS</h4>
                    <ul className={styles["footer-links__list"]}>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Entregas</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Pagamento</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Trocas e Devoluções</a>
                        </li>
                        <li className={styles["footer-links__item"]}>
                            <a href="/">Dúvidas Frequentes</a>
                        </li>
                    </ul>
                </div>
                <div className={styles["footer-links__box"]}>
                    <h4 className={styles["footer-links__title"]}>
                        FALE CONOSCO
                    </h4>
                    <ul className={styles["footer-links__list"]}>
                        <b>Atendimento ao Consumidor</b>
                        <li className={styles["footer-links__item"]}>
                            <a href="tel:1141599504">(11) 4159-9504</a>
                        </li>
                        <b>Atendimento Online</b>
                        <li className={styles["footer-links__item"]}>
                            <a href="tel:11994338825">(11) 99433-8825</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div className={styles["footer-links__socials"]}>
                <ul className={styles["footer-links__socials-list"]}>
                    <li>
                        <a href="http://" target="_blank" rel="noreferrer">
                            <FacebookIcon />
                        </a>
                    </li>
                    <li>
                        <a href="http://" target="_blank" rel="noreferrer">
                            <InstagramIcon />
                        </a>
                    </li>
                    <li>
                        <a href="http://" target="_blank" rel="noreferrer">
                            <TwitterIcon />
                        </a>
                    </li>
                    <li>
                        <a href="http://" target="_blank" rel="noreferrer">
                            <YoutubeIcon />
                        </a>
                    </li>
                    <li>
                        <a href="http://" target="_blank" rel="noreferrer">
                            <LinkedinIcon />
                        </a>
                    </li>
                </ul>
                <a
                    className={styles["footer-links__site"]}
                    href="https://m3ecommerce.com/"
                    target="_blank"
                    rel="noreferrer"
                >
                    www.m3ecommerce.com
                </a>
            </div>
        </section>
    );
}
