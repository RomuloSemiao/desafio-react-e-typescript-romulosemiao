import React from "react";

import { ArrowTop } from "../../assets/images/ArrowTop";
import styles from "./ButtonScroll.module.css";

const scrollTop = () => {
    window.scrollTo({
        top: 0,
        left: 0,
        behavior: "smooth",
    });
};

export function ButtonScroll() {
    return (
        <button onClick={scrollTop} className={styles["button-scroll"]}>
            <ArrowTop />
        </button>
    );
}
