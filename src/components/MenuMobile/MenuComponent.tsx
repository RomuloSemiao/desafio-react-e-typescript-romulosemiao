import React from "react";
import ReactDOM from "react-dom";

import { useMenuContext } from "../../contexts/MenuContext";

import { MenuCloseIcon } from "../../assets/images/MenuCloseIcon";
import styles from "./MenuComponent.module.css";
import { RigthArrowIcon } from "../../assets/images/RigthArrowIcon";

export function MenuComponent() {
    const { isOpen, setIsOpen } = useMenuContext();

    return !isOpen ? (
        <></>
    ) : (
        ReactDOM.createPortal(
            <div className={styles["menu-overlay"]}>
                <div className={styles["menu-header"]}>
                    <h3 className={styles["menu-title"]}>MENU M3 ACADEMY</h3>
                    <MenuCloseIcon
                        className={styles["menu-close-icon"]}
                        onClick={() => setIsOpen(false)}
                    />
                </div>
                <ul className={styles["menu-list"]}>
                    <li className={styles["menu-item"]}>
                        CURSOS
                        <RigthArrowIcon className={styles["menu-item-arrow"]} />
                    </li>
                    <li className={styles["menu-item"]}>
                        SAIBA MAIS
                        <RigthArrowIcon className={styles["menu-item-arrow"]} />
                    </li>
                    <li className={styles["menu-item"]}>
                        CONTATO
                        <RigthArrowIcon className={styles["menu-item-arrow"]} />
                    </li>
                    <li className={styles["menu-item"]}>
                        DUVIDAS
                        <RigthArrowIcon className={styles["menu-item-arrow"]} />
                    </li>
                </ul>
            </div>,
            document.getElementById("menu-mobile") as HTMLElement
        )
    );
}
