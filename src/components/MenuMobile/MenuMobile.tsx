import React from "react";

import { useMenuContext } from "../../contexts/MenuContext";
import { MenuComponent } from "./MenuComponent";

import { MenuIcon } from "../../assets/images/MenuIcon";
import styles from "./MenuMobile.module.css";

export function MenuMobile(props: any) {
    const { setIsOpen } = useMenuContext();

    return (
        <>
            <div {...props}>
                <MenuIcon
                    className={styles["menu-icon"]}
                    onClick={() => setIsOpen(true)}
                />
            </div>

            <MenuComponent />
        </>
    );
}
