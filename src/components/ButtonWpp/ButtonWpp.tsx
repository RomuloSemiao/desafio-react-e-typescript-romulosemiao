import React from "react";
import { WhatsappIcon } from "../../assets/images/WhatsappIcon";

import styles from "./ButtonWpp.module.css";

export function ButtonWpp() {
    return (
        <div className={styles["button-wpp"]}>
            <a
                href="https://web.whatsapp.com/send?phone=552117904-3912"
                target="_blank"
                rel="noreferrer"
            >
                <WhatsappIcon />
            </a>
        </div>
    );
}
