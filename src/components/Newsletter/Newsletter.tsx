import React from "react";

import { Formik, Form, Field, ErrorMessage } from "formik";
import NewsletterSchema from "../../schema/NewsletterSchema";

import styles from "./Newsletter.module.css";

interface newsletterProps {
    newsletterEmail: string;
}

const initialValues = {
    newsletterEmail: "",
};

export function Newsletter() {
    const handleFormikSubmit = (values: newsletterProps) => {
        console.log(`Newsletter Email: ${values.newsletterEmail}`);
    };

    return (
        <div className={styles["newsletter-wrapper"]}>
            <Formik
                initialValues={initialValues}
                validationSchema={NewsletterSchema}
                onSubmit={handleFormikSubmit}
            >
                <Form>
                    <h3>ASSINE NOSSA NEWSLETTER</h3>
                    <div className={styles["newsletter-box"]}>
                        <Field
                            id="newsletter"
                            name="newsletter"
                            placeholder="Email"
                        />
                        <button type="submit">ENVIAR</button>
                        <ErrorMessage
                            component="span"
                            name="newsletter"
                            className={styles["newsletter-invalid-message"]}
                        />
                    </div>
                </Form>
            </Formik>
        </div>
    );
}
