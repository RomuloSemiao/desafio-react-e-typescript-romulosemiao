import React from "react";

import { IconMinicart } from "../../assets/images/IconMinicart";

import styles from "./UserStats.module.css";

export function UserStats() {
    return (
        <div className={styles["user-box"]}>
            <a href="/" className={styles["user-login"]}>
                ENTRAR
            </a>
            <IconMinicart className={styles["user-mini-cart"]} />
        </div>
    );
}
