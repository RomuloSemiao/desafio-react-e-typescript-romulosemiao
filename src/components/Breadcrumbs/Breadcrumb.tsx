import react from "react";

import { ArrowRight } from "../../assets/images/ArrowRight";
import { IconHome } from "../../assets/images/IconHome";

import styles from "./Breadcrumb.module.css";

export function Breadcrumb() {
    return (
        <div className={styles["breadcrumb-container"]}>
            <IconHome />
            <ArrowRight />
            <span className={styles["breadcrumb-text"]}>INSTITUCIONAL</span>
        </div>
    );
}
