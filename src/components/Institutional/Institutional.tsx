import React from "react";

import { BrowserRouter, NavLink } from "react-router-dom";

import { RoutesComponent } from "../../routes/RoutesComponent";

import styles from "./Institutional.module.css";

export function Institutional() {
    return (
        <div className={styles["institutional-wrapper"]}>
            <h2 className={styles["institutional-title"]}>INSTITUCIONAL</h2>
            <div className={styles["institutional-block"]}>
                <BrowserRouter>
                    <nav className={styles["institutional-menu"]}>
                        <li>
                            <NavLink
                                key="Sobre"
                                className={({ isActive }) =>
                                    isActive
                                        ? `${styles["institutional-item"]} ${styles["active"]} `
                                        : styles["institutional-item"]
                                }
                                to="/"
                            >
                                Sobre
                            </NavLink>
                        </li>

                        <li>
                            <NavLink
                                key="Forma de Pagamento"
                                className={styles["institutional-item"]}
                                to="/"
                            >
                                Forma de Pagamento
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                key="Entrega"
                                className={styles["institutional-item"]}
                                to="/"
                            >
                                Entrega
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                key="Troca e Devolução"
                                className={styles["institutional-item"]}
                                to="/"
                            >
                                Troca e Devolução
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                key="Segurança e Privacidade"
                                className={styles["institutional-item"]}
                                to="/"
                            >
                                Segurança e Privacidade
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                key="Contato"
                                className={({ isActive }) =>
                                    isActive
                                        ? `${styles["institutional-item"]} ${styles["active"]} `
                                        : styles["institutional-item"]
                                }
                                to="/contact"
                            >
                                Contato
                            </NavLink>
                        </li>
                    </nav>

                    <RoutesComponent />
                </BrowserRouter>
            </div>
        </div>
    );
}
