import React from "react";
import { AmericanPayment } from "../../assets/images/AmericanPayment";
import { BoletoPayment } from "../../assets/images/BoletoPayment";
import { Developed } from "../../assets/images/Developed";
import { EloPayment } from "../../assets/images/EloPayment";
import { HiperPayment } from "../../assets/images/HiperPayment";
import { MasterPayment } from "../../assets/images/MasterPayment";
import { PaypalPayment } from "../../assets/images/PaypalPayment";
import { VisaPayment } from "../../assets/images/VisaPayment";
import { VtexPci } from "../../assets/images/VtexPci";

import styles from "./Footer.module.css";

export function Footer() {
    return (
        <footer className={styles["footer-wrapper"]}>
            <div className={styles["footer-copyright"]}>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor
                </p>
            </div>
            <div className={styles["footer-payments"]}>
                <MasterPayment />
                <VisaPayment />
                <AmericanPayment />
                <EloPayment />
                <HiperPayment />
                <PaypalPayment />
                <BoletoPayment />
                <VtexPci className={styles["vtex-pci"]} />
            </div>
            <div className={styles["footer-development"]}>
                <Developed />
            </div>
        </footer>
    );
}
