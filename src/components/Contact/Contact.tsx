import React from "react";

import { Formik } from "formik";

import FormSchema from "../../schema/FormSchema";
import { ComponentForm } from "./ComponentForm";

import styles from "./Contact.module.css";

interface FormikValuesProps {
    name: string;
    email: string;
    cpf: string;
    birthday: string;
    phone: string;
    instagram: string;
    validate: boolean | undefined;
}

const initialValues = {
    name: "",
    email: "",
    cpf: "",
    birthday: "",
    phone: "",
    instagram: "",
    validate: false,
};

export function Contact() {
    const handleSubmit = (values: FormikValuesProps, { resetForm }: any) => {
        console.log({ values });

        resetForm();
    };

    return (
        <div className={styles["form-wrapper"]}>
            <Formik
                initialValues={initialValues}
                validationSchema={FormSchema}
                onSubmit={handleSubmit}
            >
                {() => <ComponentForm />}
            </Formik>
        </div>
    );
}
