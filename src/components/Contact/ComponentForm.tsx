import React from "react";

import { ErrorMessage, useFormikContext } from "formik";
import InputMask from "react-input-mask";

import styles from "./ComponentForm.module.css";

interface FormikValuesProps {
    name: string;
    email: string;
    cpf: string;
    birthday: string;
    phone: string;
    instagram: string;
    validate: boolean | undefined;
}

export function ComponentForm() {
    const { values, touched, handleChange, errors, handleSubmit } =
        useFormikContext<FormikValuesProps>();

    return (
        <form className={styles["form-component"]} onSubmit={handleSubmit}>
            <h3>PREENCHA O FORMULÁRIO</h3>
            <div className={styles["form-box"]}>
                <label htmlFor="name">Nome</label>
                <input
                    type="text"
                    id="name"
                    name="name"
                    placeholder="Seu nome completo"
                    onChange={handleChange}
                    value={values.name}
                    className={errors.name && touched.name && styles["invalid"]}
                />
                <ErrorMessage
                    component="span"
                    name="name"
                    className={styles["form-invalid-message"]}
                />
            </div>
            <div className={styles["form-box"]}>
                <label htmlFor="email">E-mail</label>
                <InputMask
                    mask=""
                    type="email"
                    id="email"
                    name="email"
                    placeholder="Seu e-mail"
                    onChange={handleChange}
                    value={values.email}
                    className={
                        errors.email && touched.email && styles["invalid"]
                    }
                />
                <ErrorMessage
                    component="span"
                    name="email"
                    className={styles["form-invalid-message"]}
                />
            </div>
            <div className={styles["form-box"]}>
                <label htmlFor="cpf">CPF</label>
                <InputMask
                    mask="999.999.999-99"
                    type="text"
                    id="cpf"
                    name="cpf"
                    placeholder="000.000.000-00"
                    onChange={handleChange}
                    value={values.cpf}
                    className={errors.cpf && touched.cpf && styles["invalid"]}
                />
                <ErrorMessage
                    component="span"
                    name="cpf"
                    className={styles["form-invalid-message"]}
                />
            </div>
            <div className={styles["form-box"]}>
                <label htmlFor="birthday">Data de Nascimento</label>
                <InputMask
                    mask=""
                    type="date"
                    id="birthday"
                    name="birthday"
                    onChange={handleChange}
                    value={values.birthday}
                    className={
                        errors.birthday && touched.birthday && styles["invalid"]
                    }
                />
                <ErrorMessage
                    component="span"
                    name="birthday"
                    className={styles["form-invalid-message"]}
                />
            </div>
            <div className={styles["form-box"]}>
                <label htmlFor="phone">Telefone</label>
                <InputMask
                    mask="(99) 99999 9999"
                    type="text"
                    id="phone"
                    name="phone"
                    placeholder="(00) 00000 0000"
                    onChange={handleChange}
                    value={values.phone}
                    className={
                        errors.phone && touched.phone && styles["invalid"]
                    }
                />
                <ErrorMessage
                    component="span"
                    name="phone"
                    className={styles["form-invalid-message"]}
                />
            </div>
            <div className={styles["form-box"]}>
                <label htmlFor="instagram">Instagram</label>
                <InputMask
                    mask=""
                    type="text"
                    id="instagram"
                    name="instagram"
                    placeholder="@seuuser"
                    onChange={handleChange}
                    value={values.instagram}
                    className={
                        errors.instagram &&
                        touched.instagram &&
                        styles["invalid"]
                    }
                />
                <ErrorMessage
                    component="span"
                    name="instagram"
                    className={styles["form-invalid-message"]}
                />
            </div>
            <div className={styles["form-box-validate"]}>
                <div className={styles["form-box-validate__container"]}>
                    <label htmlFor="validate">
                        <span className={styles["form-box-validate__asterisk"]}>
                            *
                        </span>
                        Declaro que li e aceito
                    </label>
                    <input
                        type="checkbox"
                        id="validate"
                        name="validate"
                        onChange={handleChange}
                        value={`${values.validate}`}
                        className={
                            errors.validate &&
                            touched.validate &&
                            styles["invalid"]
                        }
                    />
                </div>
                <ErrorMessage
                    component="span"
                    name="validate"
                    className={styles["form-invalid-message__validate"]}
                />
            </div>
            <button type="submit">CADASTRE-SE</button>
        </form>
    );
}
