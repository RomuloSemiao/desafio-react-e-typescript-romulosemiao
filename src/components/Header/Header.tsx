/* eslint-disable jsx-a11y/alt-text */
import React from "react";

import { LogoM3 } from "../../assets/images/LogoM3";
import { MenuMobile } from "../MenuMobile/MenuMobile";
import { SearchBox } from "../SearchBox/SearchBox";
import { UserStats } from "../UserStats/UserStats";

import styles from "./Header.module.css";

export function Header() {
    return (
        <header>
            <div className={styles["header-wrapper"]}>
                <MenuMobile className={styles["menu-hamburguer"]} />
                <a
                    href="https://lp.digitalm3.com.br/m3-academy-universidade-corporativa"
                    target="blank"
                >
                    <LogoM3 className={styles["header-logo"]} />
                </a>
                <SearchBox className={styles["search-box"]} />
                <UserStats />
            </div>
            <div className={styles["header-menu"]}>
                <ul className={styles["header-list"]}>
                    <li className={styles["header-item"]}>
                        <a href="/" className={styles["header-item-content"]}>
                            CURSOS
                        </a>
                    </li>
                    <li className={styles["header-item"]}>
                        <a href="/" className={styles["header-item-content"]}>
                            SAIBA MAIS
                        </a>
                    </li>
                </ul>
            </div>
        </header>
    );
}
