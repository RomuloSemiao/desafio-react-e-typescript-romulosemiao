import React from "react";

export function BoletoPayment() {
    return (
        <svg
            version="1.1"
            id="Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            width="36px"
            height="20px"
            viewBox="0 0 72 40"
            enable-background="new 0 0 72 40"
            xmlSpace="preserve"
        >
            {" "}
            <image
                id="image0"
                width="72"
                height="40"
                x="0"
                y="0"
                href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAAAoCAYAAABdGbwdAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAA
CXBIWXMAABYlAAAWJQFJUiTwAAAGo0lEQVRo3u2Za2yT1xmAn/MldmycOCYkdnzJjSQklEWhg5UM
1kHLrawrWxGiLTAubQcr2irtx1YNQTsWICBBKZexds2WbmytxLpxGYKlajVNa6Fo3AlxLjh1Gigk
oRAnIU5ix+9+RPFES7ClMcXS/Pw85z3v957Hn8/3fecoERkHfAhkEGcIAV5USu1RIvIB8OhIVxSD
9AEOJSJBIGGkq4lRpmjE5dwLozbSFcQ6cUERSIwmqLGxAaNxFC6Xi8aGevx+P4mJiTidLm7evIkr
K4vaSzUA2GyZWG02Lp4/T+mDD1LndlM8fjzBQIDgQJBbtzoYNcpIaqqFT5qa6PT5GFuQz+3u21ht
NmounEcABIofeICbNz9nIBAkxWymv7+fDKuVzs5OJBTC5/ORnZNDT08PjfX1WEZbCASC5OXl4XbX
kpuXR5PHg9VqIz0jndpLlwCwWm1k2u3RGZIomD93tlSUbxARkSWLFkhJUb7Mf2y2HPrLn2XF0mek
rs4teU6b5DltsnfPLvmkqUlKiwtFRGTpooUiIlLvrpWTJ07I9i2b5czpU4N5582RwhynHHz3T/LW
byvlcmN9OE92ZrrcuNEuWzeXyyvr1srRI4fljb17RETk2JEjcvTwIfnhD1aJiMjZM6cl12GV9T97
Sdasek6uXftMHi6bLH/c9zv5SuFY2f3adqmvd0tJUb6UFOVLVeWvo5m2iMj0qP5iSqnh+74Qp77Q
IYP3w7Cj75b5jjyoe4yX4aMUyL0uHSXxNSgCcUERiAuKQFxQBOKCIhAXFIG4oAjEBUUgLigCcUER
iAuKQFxQBOKCIhAXFIH/nSAVRZuKZtDduA/7GFES1Y5iJJxOJ/sPHAYUWdlZmEzJ/P7t/QBs2FQB
gMPpwpZpx+F0YrXaAPjFpgoGgkEcrix0ukT0+iT2H/hrWEJaWjqLly5DaQqjwcDEr04CoGzaN1BK
MXbcOAAKCgrZf/AITpeTvr4+jEZjbAkyGIwkJSUBcKOtnUR7IiWlEwHILygkEOjnxImPSNIbKJs6
Fb1eD0BGRgZmcyrJKSnhXFlZWbS1tQJQc/E8ANk5uXza7B3M394OgMlkoqioGBHh4vlz+P1+CgoL
sNsd+HwdsSWo2evlO/PmEpIQoVCIBE2jYtsOnl68hIaGehYvXEBHxy2UUmRmZrL/wGEy7Q6mf30K
r/+mipmz54Rz7dyxjXf+sI+B4AAwuCP5q8oqXnh+JZqmkaBpBIMDzJg5k22v7WLFkqepc7vR6XRY
LBa279xDSWnpfRN039agUCjEoaPV1HmaWbxsOW/vewuALeUbsDudnLpwiY/PXCA9PYMtmzcCoDT1
pWVHKcXCRU/h9nhxe7zUebx86/FvU+dppnzzFnLHjsXt8fJ6ZRVVlW/S2eHjH8dPcq62gWkPT6di
4wb6evvum6D7cgcNTfbM6X/R7PVy7epVJk1+iP7+fo5/9E+279qDxTIagO+tfJbyn6/nXgtt6/VW
qquPAeCwOyibOg1TcjJ6fRIiYEpOBuDkx8dZsOgpsrJzAFi+8jkWPDGPK1daYlCQUuze8SpaQiLd
XZ1MfqgMv7+H3t5eTKbkcJzd7qC7q5tQKDRsrrOnT3Gl5VMAyqZOo2zqtLtECV0+H3a7I9xiTjUT
EqGrqyv2BIUGBnjn3YMUFRfz3t+O8eKa1SxdtpykJAPtrW3huGavlxSzGU0b/sR7/pNPsmnrtnte
TwRGmZLxeC6H2z6/cQNNQYrZHHuCUIrPrragFFw4d5aQCDpdInPnPc4vd+1g4qRJ9HTfZvfOV3li
/neHpkl7ezuNjQ0ApKWNAaC7+3a4DSA7Oyf8lPzP5RSz5sylqvJNHp01C6fTxdaKTUwoKSU/Pz82
BA2tIkpTJKek8KMXVqOUhsFoYO26lzGOMvHyhnLWrHqexx75JgkJiXxtyhR+unYdACZTChtfWR8+
v/rxT15Cp9PzwfvVvP9eNTC4+P/9wxNk2u3ok5LCrwgAz35/NW2t11mx+BlCoQHGT5jA3jcq7yzu
v/3dRSIfr9XW1GBJG43D4eRyYyM9PbcxGo2MHp1Gd3c3uXl5dHR0AKBpGjqd7ksva1daWhARnC4X
mjb48BwaM4TBYACgt7f3jvbU1FSUUvT19eH3+7FYLHf0t16/Rm9fPw67HZ1eTyAQoMlzmdRUC22t
rVgzbaSkmPFcbgTA6cpizJgx0fiZEZWg/2NmxD9WIxAXFAENCIx0ETGMaMDxka4iRvEBdRqwEmge
6WpijB5gvVKq7d/XYsR0IOkG1QAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMi0wNy0wMVQxOToyMDo1
MCswMjowMGaFIRgAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjItMDctMDFUMTk6MjA6NTArMDI6MDAX
2JmkAAAAAElFTkSuQmCC"
            />
        </svg>
    );
}
