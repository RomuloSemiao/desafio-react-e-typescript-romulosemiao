import React from "react";

export function VisaPayment() {
    return (
        <svg
            version="1.1"
            id="Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            width="35px"
            height="20px"
            viewBox="0 0 70 40"
            enable-background="new 0 0 70 40"
            xmlSpace="preserve"
        >
            {" "}
            <image
                id="image0"
                width="70"
                height="40"
                x="0"
                y="0"
                href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAAoCAYAAABD0IyuAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAA
CXBIWXMAABYlAAAWJQFJUiTwAAAIBUlEQVRo3u2Ya2wU1xXHf3d2dm2vn9gG2zxjwAaHGhoKmNgE
SkwDhFIJkwYRhBoEhTRV26hINFGrtgqq1A/Nl6ovlSpFpaGOIEg1LwOCAgmUgME24Ad+4Cf2Gpv1
2sbP3ZnbD7M7a2NvsAMJKN2/tB9m7zn3nvO//zlz7hVSynDgbWAHEA8I/j+hAzeBLUKIa0JK+TPg
t086qqcIrcBSIaVsB2KedDRPGX6uECRlJKQoTzqCpxVBYgIgSEwABIkJgCAxARAkJgCCxASAOhqj
3t5+pBx5zGq1YLUGnqand2DogqqCzaridmu4PbrxpwDVIrCNME+rs4c2Zw+d3QMgBInxdqYmRSFE
4JNL/4CGpvsDtoeOKs2xE3P8eAEnThVSdKMWUEAI70/BHmZj7aqF/PStNcP8dF0nZ/OfcN3v9fop
vLtrLS8tn80P3jnAzfIWBAIhFN75STZrV6SZvnmnbnHoRCnF5a1092lYFAXpXXfGtFh+tGk+q5Ym
D1lPSsnuvxRw6tMmdOmNUQr27l5KytSox09MTk4WOTlZNN5pI+/IFT7IPU/bvS5AweUS/PlvJ3j1
u88zeVLsEL/T58qobWgDYZAZYrORvTQVXZfU1jtxdfQYYwhmTI0zk9u5+xj556u9YwqqIsCbJ0JQ
0+AiIsI2LM78j+vJza9CKMqgDVQoqW4fMzFjqjGTJ8Xz5o7VHNy/C5uqAoZcLaqF6zfqhtl/8OEl
w8T7Hm7MWYDFolBa6aCuwenbZ+xhVlJnxAPwm9//h/xzlYP8JJou0XQdXZcgJf0DGjOnxgxbLze/
0rga8Pr5ftfK742JlFEr5kEkJY1j8aJUzl8sM8m5+GklL696zrSpqW3lk0u3UG1WQOBxa2zZ+DwA
zY4OLBZh+EpB+uxEVIuCo7WLg0dvGFMKCQhe+0462zZ+g5jIELq6Bygub+FCwR0S48OHxFRZ5+K/
Rc1YrRaMmxNjboCy264x5/i5v0ppaZO9ixvyb2/vGjJ+9GQRqtXifZIsWTiDhAmGnAuK6kxfkKSl
JgBQVXOPAbfHJNvt1tj1RhaJ8eGEhqiMj7WzIjOZX/14ybB49uWVYVUNIoSQpE6LwaeYuub7ODv6
vxxivv3yAqTul2tJWaMhdV+g/7roTV4ipeT7r79gjhXebBwi99TkeG9CBsk+P6uq8Md9l2lpu/+Z
sXg8OkfO1pibkJGewNzUWPO5s3uA+ubPnuOxEZP8zARCQ63m4vecXbS0uAA4ml9IR1e3qYiJidFk
ZcwEQNcl5ZUO00/TJN/MNMaeTU3AHmY1/UCyZ38B63bk8v6BQvQAPcO+f5fRN+DGp8L12dOZM2Oc
+WwRcL3KyVjwuYmxWlVWvDjP2HkJfX0DVNW0AHDyzM1BioDtm5eafsU3G+nr8ycRGW4jNsYOwLjo
MHZtX4bbrQ1aSeLq6ON3f73I6tf3U9PYMSyWf+SVmK90VISNjHlJLJ6b6CXSiOFq2dgK8CN1vulf
m4ZvZxVFUFraSFOTk5Oni02b6Mgwli2ZbT6XVjSjKJiq8KnFhw3rvs77771CbEyYl3R/cg3NHWx9
+zCuLn+9uHitCUdbt2mzeG4CcTEhTEmMIGRQjSu93f7lEZO9PB1d88lbcr20nkN5l/11AsmyrFlM
TIoxfS5fqxn0qkDq9AnD5n0hI5nTudt4a1sW4XabORcSHHe7eG/PJdP2n3kl5u29rkvWv2QQLQTM
S4n1+7X20D+gMVp8rs+1D1MmjycuLpx2Vy8gqKi4Q1mZr7AKNF3ne5uyhviUVzhMUpCS2SkTRpw7
xKay/bWFrFyWwpadhwxVCMOvtb0bgNrGDs5dqQcESIiwW/nweAUH8itBCBz3esxN0zTJ2QIHKzMn
ffHECAGLF83i+MlCQFLf0ApYEIrRR8yfm8yc2ZNNe7dbo7rmLqrNAlLg9mjMTzfGPR4dVR0u4GmT
YlgwdyJHzlTi602k94h17FyV8WUURmfc0+vm9KV67znK3/kiBEJImlu7R53bI5+uF2fMMnfFEIJu
fqI3bcgcYnvpajWqKsxXKXX6eKIiwwA4eLiIrTtzOX6mnIYmF909/bTe6+ajYze5eHVo3zN/TgJu
j87B/Fv+tU1Ib6MtB40Zvp8UOhgtHkkxANkvzuUXv96PxWIxu1WAhLgo1gzqhAGKiuv99UUI5qQl
mWNXrtdz4XINF67UAgKpC6SioKoW7yFJASGJi7bzyuo0Tn18m+a79w11CEFCXDjLF09h0KEKEBw+
W0tPvwZCUjqGDviRiRkfH834+Eic7d34WnEpJevXLcKiDL0aKKts8u6gAClZ9Nwz5ljprWbMNl5g
HBkUzHqFkExKjOYP764hbpydv39UjBg014bVs3hj47xh8dU1d3Hp+l1A4uzow9nZT2xUyBdPDMC3
sudxLL/QfKd1HTa+OrxtLypuIDIiFIRAl4JnUxMBo/9Im5mA1apSUd2GLkEoOrpHISRUYfLEGJYs
SubNzRnERIVyx9FJdZ2TyHAbINAkrF+ZMnJsmVMoqXJ6a41CeY2LzHkJD81JSBnoCmr0cLs9DzRl
YLcP35UHL63CQq1DLpyklDQ5Omm+24mmG6fucdF2JiZEoQxSn9ut4dZ0/0QSo2MeAR6PzoDHb2tV
FazqQ0vr3sdCzFcQe4N3vgEQJCYAgsQEQJCYAAgSEwBBYgIgSEwAKEDLkw7iKUSFAux50lE8ZWgC
clXglxjK2Qo8/BDx1YUGlAA/FELU/A+4q0mVSqgteQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMi0w
Ny0wMVQxOToyMDo1MCswMjowMGaFIRgAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjItMDctMDFUMTk6
MjA6NTArMDI6MDAX2JmkAAAAAElFTkSuQmCC"
            />
        </svg>
    );
}
