import React from "react";

export function PaypalPayment() {
    return (
        <svg
            version="1.1"
            id="Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            width="36px"
            height="20px"
            viewBox="0 0 72 40"
            enable-background="new 0 0 72 40"
            xmlSpace="preserve"
        >
            {" "}
            <image
                id="image0"
                width="72"
                height="40"
                x="0"
                y="0"
                href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAAAoCAYAAABdGbwdAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAA
CXBIWXMAABYlAAAWJQFJUiTwAAAHt0lEQVRo3u2Ya3BUZxnHf+fsOXv2mt0l2VxIk0AISVoQY4DA
IE0oyAgttIg6lIJQp8JQdaZ+aGcsU7TU0XZ0bHVqp44dsRU6TCl1tIDKZKpGhHIZQ8qdkPuVhCyb
ZJPd7O0cP5ywYdnAMiMQprP/b+e9Puf3Ps/zXgRN0xYC+wAnKV1TFHhVEIRtgqZpp4AvTLRF96E0
wC1omhYFxIm25j5VpUgKzq0kpuAkUQpQEqUAJVEKUBKlACVRClASpQAlkXRjgaZp1Lf1o0bVhMai
QcRlV3C7zAiCMNG2Twygw591sXjT+4QjiYAQwKLIFOW7+Ml3K3m8qmii7b/rSgixTz/rHB8OgAb+
kTCn6nt56sW/8Mmx1om2/64rAVBD+9Xb6jgcCPOr3f+daPvvuhJCrLFjIL4gGgTtRo8SQBBp7x7g
8644QFFVo7HdO1agaQhDPTft3OZ3cKQ3yIJMJaGu2zPMS7+pIRgeg2s1SVTMzGXjihlI0p3bQLef
7OfSYCT2LQpQYJPZXGojz2K4rTEOdgTY2ThMnlXi1TnO8QF5B0fo6w+MFaiRWw7aX1zMliMeDq/I
wS7F72rHT3ez48+nEvr87qM6Pqw+x9/fWnNH4IQ1+OXZAXzBxLy54+IAB76aQ9kkOek4e1uGeb/B
x2P51rjyuGXs8fgZ8gfHCrSbAxJMJrSiaZzxBGkbSmxXV9+rr4Bk4JVnK3nh6fk47bqnHTzSTFuP
744A6gmoMTjL8qy8PHsSFW4TAF3+KL+vv715mnxRAGa7jXHlcR5U3+qJ7xUJjj+aJKMtWwpGGVEU
kMTEM9HxM50AFOY62bZ5AQC9Hj/v7dO9KhiK6meuVi+NHV5cdjNfKs2kvWeIqKpSnO+iu28Inz9M
usOE22WJjd11ZYjB4RAOm0JDZCyENpXYWV1g5pFshaoD3fo8EQ0AT1DltDdMSNWY5ZRRBRgMqbgV
EYss0ugLAfCg4xaA6ur74iqFaCjhxwWTgva1lWiFUwEodhh5YJw4b2zrB2BqrkNnHVGpPa8b7bKb
KMiys+aHH7Ov5hIjQd0DH5rmpqnDy0gwwj/fWcdv99bywcHzzCxyU7v728iSSHffEOVrd9Dj8fPc
U3OZtWoeAIpBYIpND6VDPWN2V2SZeLdhmK0nrtLt1+exyyKTrRIX+0M8VmDjF3Nd9AV0L5xqj9+3
4r5OnO2K/8s5ZeC0gSCALEN2Fmpmhv5aO6rVBRasN+SfYFilsVMHVHv+MhXr/0hjh5erA3p++87X
y1i79WP+9MlFTIrEM6u+iEES2bX/dAxWUZ6TRxcW8cHB89S3XKW+1UPJlAw2/ugAPR4/5Q9ms/3Z
hWw/5wdAFAQ2Hb6CL6RyaUAHVJRmpMgu8ZW/dhHVoCLTxIJMherOEc569eiYk26kcTDMcERFAIrT
4vNVHKDmjut2MFlCWzgPlHiXux5OgU1ic6k9wXuaOrxEInpMX/EOc8U7DECG08qGFTNYv3wmc9f/
ARD42fer+MG6uQgC2Mwyr+88jt2qkJ1upWpOHnaLgs8f5HBdJzW1bVR/2oxJkXjj+SU4bAon+nSb
AxGV2t4RQEM2iFRkKry9IINttV6iGlTlmPloSRbpikCTL0LRnnY0oCzDSMtoDs20SjiN8YsdAxSK
qDR29I+FksWEdiOcURkEgeV5Fn49fxL51sTwau4aOx/9eEslVbPzsZllpuQ6cDvN7DvUQCgcxZmm
sLJqOteudYpRN2dKjgNJEinITmPV4uns3H+GXQfOcqFZz5EvbKygsjyPQFTjQr/uLZU5Zl4udyGJ
AvlWA5PNIiEVDl8eAWB5noV0RZ/IJosg6ItdZJep7tA9u9AmcWM2jQFqau8nPLrqgB5a16ky28yL
ZU7SZJEShxybbDw1tI154tplD1FS4Iqrtxh1N/YNh/mw+gLfW1NO3YUe3t5Tqxua54y1Xb24hJ37
z3DoZDsAZSWZbN/yMAB9I1H6R3ewJZMtPJIdfx6LaBqKQbfzb+1+1k2zEVE1Nv2nD00DqySQYzbQ
MKhDLrQnHgdigJq7+uMqtLT40Hlymo1luSZuR+ea9GRvNsnkZ6cl1D9c/gA5GTa6+4bY+ua/+Pm7
RwlHVfyB0dyRNwZ0+ZcLMcoGQuEoWelWdr/2ROwlocUXJaLqMV/qSLgUYDYIPD3dzk/rvNR0Byja
04pBELi26WZZ9JBq8ekhVupMjJjYqJfarhKXYBzxB6Zy9/jhNp4Kc12srJzOrGI3ZiUxBI2ygf1v
fpNtb/2bli4fU3MdfGvFTPZWXyAcifKNpSWxtudbPDG3f37DPEqnpMfqVGBlgRVREJjnVsa15aUy
B/6oSs3lEdAEluaasBsNHOsdYX6mCVGAsgwzM1wKTxSYE/oLmqZpoN/SO3qHYhUbTgY41qdneqss
0Lomn3Tl3r6vXfH6efy5vRw93UXV7AL+8c6TiPf2HWpRzIMsJpnifN21NeBijQ90dkw2yziN9/7x
8fVdxzl6uhNXmpn3Xnn0XsMBrvOgG9UZUAmNXm8yjAJ2+d4b1+Px4w9GcNqMuNJuL//dYS26KaCU
AFiUerRPohSgJEoBSqIUoCRKAUqiFKAkEtFP7CmNL1UEzky0FfepAkCDCDwDeP/PwT5vigKvCYLQ
/T80krUEFHoNYQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMi0wNy0wMVQxOToyMDo1MCswMjowMGaF
IRgAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjItMDctMDFUMTk6MjA6NTArMDI6MDAX2JmkAAAAAElF
TkSuQmCC"
            />
        </svg>
    );
}
