import React from "react";

export function EloPayment() {
    return (
        <svg
            version="1.1"
            id="Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            width="36px"
            height="20px"
            viewBox="0 0 74 40"
            enable-background="new 0 0 74 40"
            xmlSpace="preserve"
        >
            {" "}
            <image
                id="image0"
                width="74"
                height="40"
                x="0"
                y="0"
                href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAAAoCAMAAABuMpwSAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAC+lBMVEUAAAD/////////////
///////////////////+/v7w8PDIx8evra6vra2wra7Ix8jy8vLY1teBfn5KRkcoIyQmISInIiMm
IiNBPj+HhIXOzc64trdRTU5NSkuysLHf3t9bV1hVUVLX19e2tLUxLS42MjKjoqOfnJ2Wk5SjoaIl
ICGlo6TMy8spJSaysbHq6upBPT4/PDyPjY2DgYHl5eU2MjPh4OCOjIwpJCXy8fHi4eIjHh+GhIRF
QUH9/f3s6+w5NTb7+/vW1tYwKyx3dHWtrK2FgoNBPD50bxuyrBCzrQ92cBs3MSLQz8+opqZPS0zl
5OXr6+snIiQkOEEnISKCexb/9ADk3QVZUx+bmZp3c3Q8Nzjs7Ozi4eGcmZqSj5DT0dJhXV4kPkkF
q+EbVmxFPiCVjxWZlBXj2gbv5gRFPyJyb29eW1unpaXV1NUtKCnm5ubKyckpIyQOi7YDreQShKkw
KyTOxgusphEmISNJRkZeWVre3t5oZGZkYGHBwMHo6OiQjo4sJygnLjMHp9sDr+YkOkJXUiC2rxB/
eRhDPj/v7++bmpvg4ODJyMhqZmcqJSYkNDwEreQEruYnJiliX2De3d34+PiurK1STlAmKy8HpdkD
ruUiQ1BoMyfeSSihPihLR0d6d3iPjI3k4+SIhoc3MzRNSUooJCQnISMTfqIPj7onIiJCKSXbSSfx
TCeTOid1cnMuKSokHyApJCbY2NjDwcG1s7T09PRQTE3u7u5oZWYmMjgKnc4gSlpGKiW1QSi2Qijy
TCfZRyc3JiSYlpZqZ2jU09NOSkt9e3vh4eFLR0gmLzWMOSjwTCfyTSi9Qyg4JyUlICLS0dL8+/tU
UVF/fH1aVldfW1xJKiV2MyZ3MyVPLCYqIyQ0MDGMioszLi8tKCrj4+ONi4uGg4Tz8vJCPj8/Oju9
u7wrJifGxcWlpKSNioqUkZKLiYmrqao+OTqopqfg399STk/Y19dRTE20s7NFQUMoJCVFQkKAfX7U
09Tz8/Ovrq6ReMteAAAACHRSTlMAcX3j6v7ifvhdGP0AAAABYktHRAH/Ai3eAAAACXBIWXMAABYl
AAAWJQFJUiTwAAAAB3RJTUUH5gcBFxQy+UXaagAAAolJREFUSMdjYOSgDmBiYGCmklEsDAysBJRw
cnHz8PLxCxAyipWBAb85gkLCIqJiYmLiohKSUpx41eI1ilNaBmQMDIjKynGSaZS8ghg6UFQixyhO
ZRUxTKCqRoZR6mJYgagGJ6lGaWphN0pMS5tEo3R0xXABLT2SjNI3EMMNDAVIMcpIDB8wJsEoE1PU
oBZFM8uMeKPMUTRaWFpZoxplQ7RRnLYozrGws7dGdZ2DI7FGOUG0OLu4Kru5i8CN8vD08vaBmOlL
rFF+YPX+ASB2oIso2Kig4JDQsLCw8AiwXCSxRkWBVEfHcMTGxSdwcCaCjUpKTklNS88Iy8wCSWYT
a1QOSHUuR16+mKh/AUdhEdCokOKSUqBgWXlYRSWQriLWqGpQmq7hqBWtq7fjaGhsAhrV3NLaBrKg
vaMTRHWRZFQ3R08vB2df/wRwsE+cNHkKyIyiIHGSjJoKMmoaB8f0GTO1oIlh1uw5c0FmzJu/AEQt
JNaoRSDVi5csNRUVXbZcTGyCXeCKlatKVq+prFy7bv0GkORGYo3SBKnetHnL1m3bd3TvFBPdxRG4
e8/effsPHDy0/vARkORRYo2aBkmix0CF3PETYmInT3GeFjsTcvbcufMXLoJkLl0m1qgr7pD8cXXj
tetNIEb+DVCY3bx1+85dsMQ9TmKN4rgvhh9IchBtlP4DvCY9fES8URyP8Rr1hIMEo54+w2PS80BS
jOJ40YTTpJevOEgyiuM1zsrrDQeJRnG8FcVqkug7DpKN4nj/AYtJKh85yDCK45Mihkmfv3CQZRQH
h/RXFL+d+EZuowgEfN99B9czlT9+/vpNqKlGqAHJ8Yf7Ly/fKy5Cylip16xlY2CgVmObnQEAEk4L
KxSXYpsAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjItMDctMDFUMjA6MjA6NTArMDM6MDAlzWlOAAAA
JXRFWHRkYXRlOm1vZGlmeQAyMDIyLTA3LTAxVDIwOjIwOjUwKzAzOjAwVJDR8gAAAABJRU5ErkJg
gg=="
            />
        </svg>
    );
}
