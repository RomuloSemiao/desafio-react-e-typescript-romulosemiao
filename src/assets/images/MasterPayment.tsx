import React from "react";

export function MasterPayment() {
    return (
        <svg
            version="1.1"
            id="Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            width="35px"
            height="20px"
            viewBox="0 0 72 40"
            enable-background="new 0 0 72 40"
            xmlSpace="preserve"
        >
            {" "}
            <image
                id="image0"
                width="72"
                height="40"
                x="0"
                y="0"
                href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAAAoCAMAAABqx0wvAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAC5VBMVEUAAAD/////////////
///////////////////////////////////////////////////////////////////80tf3hZLz
SVzwKD7wJjzyO0/zTmD6qrP+5+r++/X96MX6y3j5vFH5szv5tD36vFH6yXX96cX//v794OPyRFnu
Ax3vECn2fIn+9fb847j5tkH4qBz5tkD95Lv6rLXvECruBBz5XCP5oBj4qB37zX/6sbruBB76SAj/
agD+cgP5oxn7ynT93+LvEiv1LhD9eQf83KbzWmvwDBr+ZwD6khH5riv++vD82t7uBR/3Ng39dwX8
1ZP3jZj9XgL5nxj6uEbyRlnvCBv6kBD4rCb+9unxNkvyFxX8hgz+79XwKUDyHBX8hQv96sjxN0zw
Ehj7iQ3+79fzXGzvBhz/aQD6lhP5ry//+/T5qrP7Twf+bAH5oxr6wFz+7/HvEyv0KBL8fwn95bv3
hJL8WgT6u03//PzxMkfxExj+aQD7iQ75sTT+9OH++fL+6+7wK0DzIBT+ZQH8gwv5riz+7e/0XG7u
Ax75lor5szn5tT/+8dz5r7jyPVL1ann96Ov/+/f81JH5sjb5szr815j++/T94eX5rLb2f4z1eoj3
g5D6w8n+8vP+7dD826P70Yn70Yj82Z3+79b//v37+/v+/v6joaH8/PyPjI3W1dV3dHR2c3SLiYlq
Zmfj4uNxbm5+e3ywra53dHWUkZLg399cWVmhnp/Hx8eIhoa8u7uHhIXHxsZ7eHmZl5e2tLSJhodj
X2DIx8draGjAv79saWmQjo62tLW4trbJyMiQjY6+vLy+vL3b2tqqqKmsqarPzs6NiouTkZGRjo/S
0dGtq6va2dqFgYL4+PignZ6mpKXd3N2koqOGg4Tm5eXNzMzU09PX1ta1s7Tx8PCamJjHxcabmZnk
5OS0srOLiImyr7DQz8/i4eH29vahn6CMiYrf3t7Ew8Omo6ScmZqqqan09PTqRSR+AAAAFHRSTlMA
Jur+bL34Evw2P/o+wPAWIeD1bQbpob4AAAABYktHRAH/Ai3eAAAACXBIWXMAABYlAAAWJQFJUiTw
AAAAB3RJTUUH5gcBFRktwmd9vAAAAiBJREFUSMdjYGQSoRwwszAwsFLBHBERNnYGDqoYJMLJQB1z
RLhGpEGiYuISklLSMrJAtpy8gqKSsoqqmjrJBmloakGBto6unr4BFBgakWiQsYkWHJiamRvAgYWl
OikGWVkjzNGysbWzR5hk4ECCQY5OSOY429rauiAZZOBKvEFuSOa4ewANsvVEMsjLm1iDfHyRDPID
mWPrj+ykAGINCkQyRysIbJBtMJJBIcQaFIpkThjEHNtwJIMiIok0KArJoGioQTHIfosl0qA4JIPi
oQYlIBuUSKRBSUgGJUMNSkE2KJVIg9KQDErPgBiUiWROVjaRBuUgx1ou2Jy8fCSDCoiNtcIiJIOK
wQaVIPuslFiDRMqQnVQOchByMqpQJ9qgyiokg6prbG1rkcypq28g2iCRxiYkk5pbWpHMaUvEqgNX
edTegTCos6sbYU5PrwhJBomI9PVDjLGeMFFk0uQpEGOmTpsuQqpBIiIzZs6aPWfuPDBbff6ChYsW
L1mKUzE5tcgyUgxavhyIQbTICjBjOUgEIrByFfEGrV6zdt36DRs3bd6yddv2HTt3rdm9Z+++/QcO
Hjp85Oix4/tIMOjEyVOnz5w9d/7CuouXRC5vvXLg6rXrIjdu3tp7+87dZfeIN+j+g4ePHj/Z+vTZ
8ysvXj598er1m7fvTr9/+2Hdx0/LP3/5+o1KgY0VDGuDuKljEA8DL1XM4eNnEBCkgjlCwgwAwkKG
k7yZog8AAAAldEVYdGRhdGU6Y3JlYXRlADIwMjItMDctMDFUMTk6MjU6NDUrMDI6MDAePsVlAAAA
JXRFWHRkYXRlOm1vZGlmeQAyMDIyLTA3LTAxVDE5OjI1OjQ1KzAyOjAwb2N92QAAAABJRU5ErkJg
gg=="
            />
        </svg>
    );
}
