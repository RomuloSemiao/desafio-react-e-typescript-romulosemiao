import React from "react";

export function HiperPayment() {
    return (
        <svg
            version="1.1"
            id="Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            width="35px"
            height="20px"
            viewBox="0 0 70 40"
            enable-background="new 0 0 70 40"
            xmlSpace="preserve"
        >
            {" "}
            <image
                id="image0"
                width="70"
                height="40"
                x="0"
                y="0"
                href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAAoCAYAAABD0IyuAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAA
CXBIWXMAABYlAAAWJQFJUiTwAAAGZElEQVRo3u2YXWxT1x3Af+fasRM7Nkmc2PFIC3Q4SnBCykdb
VQu0VKwUWrZ2q5g2bQ/dpnUfrVbtYaAx1m08bNoXrFNQNzqV8rCyh25rl4XCVmgA8TGmJEBDSEJw
UoekieM48Xds33v24ChFGu5io8Vh5Pdwpav/+Z+P3733nHOPkFKagR3As0A5ILgz0YD3gGeEEG1C
Srkd+Gm+ezWP8AHrhZQyAJTkuzfzjJ0KC1JuhkvJdw/mKwtiMrAgJgMLYjKwICYD+luuQUr8x98l
em0AqWkZisiM6UIC3DwuBQidLlPDmdJm+nUjhXctxr75sbkRM9neQdvTX0CdCIGY3xvmsg2NcyMm
1HmZts98HjUUmfdSAOxbH8+qfM5zzMDL+9NSbgOk1DC7lmeVk7OYaHdvvsc7a3RGA+bqORCT8I8T
uXL7iLHUuzHYbFnl5CQm0t1NKhIFQLfIimI0zsR0hYXoy0qRmkrRPUvzIqKwajHW1fd+eL9sSdZ1
5Cam5ypCCKSa5OHuDqwNdTOx5T/6HtW7d1Kx+VHc+/b8x7I5Fzi3PUnF44/O3Ns3b8q6jpxWpdHm
FgCMTidCryfS55mJme5ZwvjJ00ycOcfF9g50FjNaMonRYUeqGlPXhwFQCo0YnQ6khJinnwKrFZlK
UFBhJxUMo8WiGJ2V6IqKCF3uwlBehr64GJ3ZTKizK92+o4JUJIqhtJSYd5CixU701kUU19TgP3Ua
SO+hjJWOuREzNeoDoOjuKlKhEAa7DUNlBUiJddUqvK8cxLX7+0yeb8OxdQsUFRH6Vzv2T22h7ye/
IHD6DPX7mxg/fhL7E5sZfP2PEJ9i6befIz48zPAbf2LJ159ltLmFQqcTz69e4u7nvkZixEfJ2tVo
qRTd23dx35G3mLxwifHWVkpXr0EUGoh5vJQ98hDvv/IqAEIH1nsb/vdi1FiMaM9VACwN9cSvD1Gx
Kf3aGsptFJSUEOnpZfmuHXh/d4C7vvoMXS98l2DHJbSpOLYN66h8aivBi51EPf0E2y9iratDi8cY
/vNf6Pvxz3jg3cP0/nA3Y4ffmW5V0r+nCYu7Fi0ep2zDQxjt5aRCIS5+8cs4t30WxWyifduXUIwG
Kp7YRPDCJSAt5cY5cLZkPcdMnD2HTKrTja7E9/ZR+vc20b+3iZE3/0pycpJoTw+mZUuJ9HRjsFcQ
mRYJkAyGKG1sJNTWgRafYuxYK97f/p7iFbX43/4HhVWVFNfW4Gv5+0zO0he+Sd2+lygot2Fd1UD0
ah+mmmomzp5HJlI4n34S3+EjCEWHudqFGoujxWIAFLtrspaSk5hw5xUQYnrT5CI+ODQTK15RS7Sn
F1O1i8jVPswuF8lAgFQ4hKapOJ76NOPHTiAUhbHjrfhajuJrPkyw4wIW9wqiA14UfQEykUQo6d20
1DQ+9rltXPrG8wwdPITQ6Ql3dVO2rhF/aytoGsaqKpITk+mHtbqBuNeLUNIfQ+mDD+YkJutPKXDm
bNqooQBLvZvg9p0zsbL1jYSv9FC2fh3Ra9cwVS+nwGajbv8+CkoXMfK3FsaOvsPIW82seeN1wpe7
0JeU0PvibrRkisQHI6gmM9E+Dw1/eJWkP8D4iZPErg+x4je/JOkbRTEUEO7swrF1C/GBQVAUBl87
iGvXDmyPPIyhouzDPZaUFNe7cxIjpMxuPT113yeIe7wgNQxOB1PDo4jpfyXFZAShIBSB1CTLvvM8
Sd8YgdP/JBkYJ/b+9fQyr6lY6t2osRgJnx81HMbosDP1wehMO5aVbpKBAFNDIwi9HlP1x4l6PAhN
glBQzCbUcAQtGkNqKsXuWtRIlMSYHyEEaiSKRGXjqBehZP1hHMhKTMI/TqurHqHoZlV+zZuHuPbz
PQROncvpqd0q1jUruf9Icy6pB7JSOXn+/KylICX9v24i2jeQFykAJWvX5pyb1RwTeu/y7AsLgf/Y
ybxJATDXunLOzeqNidxGf9SalqJ84ydzzs9KzPiJU/ke76wp37gBY6U95/xZi0kFgyR8/nyP978i
pUZJ4wO4m/beUj2znmOSwRCuF3feNCYznGyKG66ZCnzUoaj8iCPTm9Wts5hYdP9aLLW1tyQFctjH
3CFkt1zfSSyIycCCmAwsiMnAgpgMKMBIvjsxD+lRgP357sU8Ywg4pAd+QPrN+QqQ/XH6/w8q0Al8
Swjh+Teg83D/cJploQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyMi0wNy0wMVQxOToyMDo1MCswMjow
MGaFIRgAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjItMDctMDFUMTk6MjA6NTArMDI6MDAX2JmkAAAA
AElFTkSuQmCC"
            />
        </svg>
    );
}
