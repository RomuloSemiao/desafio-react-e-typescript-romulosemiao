import React from "react";

export function FacebookIcon() {
    return (
        <svg
            width="36"
            height="36"
            viewBox="0 0 36 36"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <g clip-path="url(#clip0_2135_307)">
                <path
                    d="M19.5769 28.063V18.9297H22.6347L23.0935 15.3693H19.5769V13.0964C19.5769 12.0659 19.8612 11.3636 21.3375 11.3636L23.2173 11.3629V8.17828C22.8922 8.13594 21.7763 8.03888 20.4775 8.03888C17.7655 8.03888 15.9088 9.69784 15.9088 12.7438V15.3693H12.8416V18.9297H15.9088V28.063H19.5769Z"
                    fill="#303030"
                />
            </g>
            <circle
                cx="17.8641"
                cy="17.8641"
                r="16.8641"
                stroke="#303030"
                stroke-width="2"
            />
            <defs>
                <clipPath id="clip0_2135_307">
                    <rect
                        width="19.9812"
                        height="20.0241"
                        fill="white"
                        transform="translate(8.03883 8.03885)"
                    />
                </clipPath>
            </defs>
        </svg>
    );
}
