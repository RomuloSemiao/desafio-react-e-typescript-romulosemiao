import React from "react";

export function VtexPci(props: any) {
    return (
        <svg
            {...props}
            version="1.1"
            id="Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            width="53px"
            height="32px"
            viewBox="0 0 106 66"
            enable-background="new 0 0 106 66"
            xmlSpace="preserve"
        >
            {" "}
            <image
                id="image0"
                width="106"
                height="66"
                x="0"
                y="0"
                href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGoAAABCCAYAAAC/3kPMAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAA
CXBIWXMAABYlAAAWJQFJUiTwAAAOCklEQVR42u2beXRV1b3HP3ufc3MzktzMhIQpEAISBgGBCFEU
oxVFqIiFqs+KVrt49Tk9tcvSPldX29XX50AXYm19WHF4YkEUB9AyCCEgIJMoJAYIU0JCbkKSm9zx
3LPfHxeSXBKbCjGXyP2slT/yO/vs396/793D2QOE6RGIcw2ZEx+OitNVAkSHumyXLNWxlua61U83
trUFCZV344K8qfnD/mdw/9R+oMS3yz5MV1FX31y1pmjfos3Lfvn3sza9bYLRuX1+8dA9hYV9eyeG
uqyXND6fP8fj8dkOVT+64eSnz9gBZNsEUpOj+qQmhLqclzwWi0ZWRlJGpDCSztrkhWQYpvsIC9VD
CAvVQ9AvPIvzwyyrxHj2fdTxWlDq/DKRApGTgWXBbYiEmFBVpVsImVD+5Z/hX/X5+Yt0lp2HkQPT
0effEKqqdAuh6/pio4ALFAlAl4i0XiGrRncRMqG0G0cjMpIuOB8xLAs5JS9U1eg2QiaUHJCGLBwJ
4vwXQISuoc8cj0iKC1U1uo1uH6OU3YE6XAWGiczri5kYi8iwgUXrOP1RO6rW0eEzMTQT7aaxrWkN
P5ysh7jIlsmFcrig3gnxUYEfRb3znAhIRG8bqt6Jqq6HJjdE6IEyRUVAbRPERiJsMSi/CY2uQJrU
Xgirpdvi1u1C+X63Av+yzS3Dk5yUi3XZo9/Ysvyf7MH7k0UdDmf6nQWIPm2Wu2oacU94Au32SUQ8
ezcA5tZSvPe8gOXxGajKOoylG4PyELZYIl57EP+qHRivrAefHwDt1gno91yL946F0MeG9eX5IAWe
eYvhdDPW5Y9B3+Rui1v3z/qkCAq6KqlEHbMj+qW0T2sq/B/v6VAkOSYbOWN8kE30tiFyMzGL9rfY
/Kv3IKwRyElDUW4flux0/Cu3Ye45gv7A9YiBaSjDj/+97cj+qWj3F4Imkf1TkaMHoM27FuOZVfgW
foCw6KiSE1h+fweiG0WCEIxRljsLEPGtWyjK3ojx+qYO05pfHcNct6/9AwH6vdciYiPbPdImDUVV
1GGWVaLcXswtJYh+yYjMZLT8Iej3XYfIyQikvXUC+h0FiGgryuUNdKW3jEO7aQxy9AAA9J9cg8zp
jX9ZMcbrG5GjB6DfOrG7w9b9Qonh/ZBThgfZ/Cu2og6eDE7oNzGWFaNqGtvlIYdlIa/peKYnJw8N
vL5mL6qkAlXrQAzpg0jufMLh/2gXnoIFeAoWYDy7KlBeWwz63VNASkRCDPp9hWANQUfU7R41iT63
AJHS+u2jquoxXvwkKJlZWom5Zk/7D2Ih0B+7BREX1XH24weDRcP8vAxzexk4Pcj8nECX2wkiOw1t
dj7a7HzE2EFnCqcwSytBKdTpZsyyym4PGYRoei7HD0a7eVyQzXh3G+bRmsA/psK/9FPUybr2747L
Rps64pszj4tCjuiPOlCB8cFORGwUWt6ATgokQArkoHQsj8/A8sTMFh9mSQXG37cg+6ci0uLxLytG
fXW8+2PW7R4BLBranQWItIRWm8uLf9HqQHD2lmO8tbn9JEII9Eemg/bPi61NHYE6UYvadRiRnQYD
Uv5pemxxiKRemJ+V4ZnzHJ65z2G8sAbl8WH8YSVCgf6r2eg/vQ5VXY+xeA3Ka3RryEL3wTu4d2Cs
ajMtN97bjrnzEP4l66CDQMi8fmj5uZ3mrc0cjxw/CJGTgT7vmqDJC4DMSkYMz4KIwHeQTI/H8uA0
SEtAHa5GHalBNbsDY5zDhXbX1WgTc9BuvxJ5w2jMUw1QdrLTcnQlIVuURUr0Bwoxt5aiznZ5TW68
c59HNbnbp4+2Ynn4ZtA7/22JvslYVz4ZaJEdDE36o9PRH53e5gWBdttEtFkTwG+C3vrxbV3xeGsy
wLr4p6EJV0i8nnU+MA3t5rFBNu2haYHu6ty0VwxGXvct1/S+7eqUEEEiXUyEduNQ19DvuhqRHJgB
yhH9sDxwPfq91wWPQ9FWLPNvAHnp7nOGvOaiTyLavGsQyb3QH7sl0A3dMg4xILW1kOMHIfOHhLqo
ISV0Y9RZhMAy/0b0OZNbWpaIj8by1Cx8T7+NTItH//dpF7TK/n0g9EJBYAU7NT7IpF0/CpmfC14f
IjE21CUMOReHUN+AiIsEIi84n+8DIR+jwvxrhIXqIYSF6iEEjVFF20vIufbx880rTBeiUEHNKNyi
eghhoXoIYaF6CGGheghhoXoIYaF6CF2/hCTO2UEXKnBtWwSubysZ+EML2JUGphZ4Zp55FmOXyO7d
6b7o6RKhDKvCFwNoClOCqbcRRQcEmAKQZ+7aSzAJ2Fv+AK9hoEkN4fETe/ri3MALFV0jVCQ09Vbf
fke1DX7T5E9zZ3PVkByWrC5ixeufoYwuuJbzPaFLxiirQ6B5zv99UyliI63cNXE8Q9JSmXXVWDRb
8AF8KSXWiIt6sf87pUtqLkywNgpcVoUpwG8Y54xTEl2TLQ1OKYXfNAPdn1JIBE0eL89+vJb8Qdm8
unUb9mgX8VIjxmqlYHwOw8amEJuos3tdDe+v2x3quHU7XfYTjawXuBMU0goPXj+VnLRUUAqv36Sy
oZ5Vu7+gpPoUKMWwjHRmXD6KoenpWHSNryoqWfjJehCCY6dPE6nreGJAt1m4a+YEHLnH+LCpmDkJ
t9DgaL02o2saP7p5AtOmBO5ZvfbOZj76dC8b33qKU7UNABw6WsP7a3ex4MEZOJpdaJpk95dH+d3i
VSz81R1kZSQTabXQ2OTkxTfWM35kNkU7SkhPSeCe2VfR7AyciPrzGxu44arhfFL0FQcOVvLG8z9r
8bGv5ATFO0t5ZN6NON0ePD4/Kz7azqq1u/CbXdN9d5lQmhcsbvBZBfnZA5k2YjgOt5vyGjtDMyYw
54qx3PT8YjJtNv427y56JyRQ1dBAo8tNVkICb2wq5qYRwxmRlYlpKpZu3UZBYS6uoZW8ffJDnhz4
M7Ysq2HT9tIWn3m5mRROHs6ipf/AomvMLBxL8c6vsfWK4oGnXgHA7fWRGB9DbIyVh3/zBhaLxq//
YyZj8gbwwmvryMvNYvq1o/jtC+9TeaqeqVdeRmx0JLHRVj7/opz/W7UVgCp7PbN+MJa4MxcT2vpo
aHIxYkgmjmYXTy98F1tCDD+ePhHDb3ZZ6+/STj+iUWDEBbo2gA0HSvnl2++w8uH55KSlUXjZUP5t
0kT6JiWysbSMn7/+FnXNzdjiYjnhaAp0hYDP72dkZh+umJTAn48v5f7+c9i0tIqNn5UG+Zs7fSIf
rN9N8c4yhBB8vq+cJqcHw29y4FDrGfH4uCicLk+Lbfuewwzqn8ab720lOTGWxmZ3UPqz2E87OrQD
7XyMGJKJ0+VtsdniY7hzxpUXp1CWJoHwtzb1ASkpPH3bTPrYbDi9XqoaG+mbaANgY+nXlNXYsega
9fY6fIav5T1bdDQPTstnSfUrjIsfgWVfKpu2rW3n77LBfXjzzC9eKUWTMzCjiY2O5OmHfgjAsUo7
e/YfAyAz3UZMVCQFV+TwXwtXdlqfKROGkpQQOK+x8uPPg5619VF6uJL6xuCbjF+WniC7b0qnPkIi
lGZAhLN1jj4gKZFmr5cPv9jHa59tZ/uhck47naT16sXVQ3J4aWMRzV4fURE6XtHaEqcMy+Y3+xfh
8ru4zDuK/11e3PKsLYbhJ6KDK6Ven8HeksBB/qZmN0II0lPi+eMv5jAyN4s/vfoJO74o77Q+1faG
lnxcbu83+qiqaSDhnNslUdYI3B5fpz5CIhRAZKOAwO1KPv26jFkv/hUpJUIITNPkhfWbePb2W5mc
M4iiJx/jaG0tKXFxHLbXkBQTuHe7tqaY1Sc3c3vmjWzdcIpqp4Mo2guyZddBxuYNZNuew2hSMnJY
FvtKTuD1GbyzZkdLulHD+lJV08Dd//kXfvvoLCIi/rW7t/sPVgblc65QbZ/dUBB8infi5dl8WVpx
8Qolm2D/wQqioiL4srISIQT6mROumqaxZPMWjthrmT3ucoakp9E/OYm3T73Le9XreS7zCaLc8NLB
t7BZezGgNpfla9YS5e14leL1d7ew8Nd3IARoUnLd5Dx+/NBiIiMszCwcA0CDw0VTc2Dm5vEa/PGv
q1m++Oe8uqKIBofrvOvZ1kd1bSOalCQnxjGzcAxpKQn8oCCPp55Z3mVxDYpAYv+J80FcUMcqTPj0
YBlLvtxGUdkhLFpwkKUQlNtreW/3XpYUFbPhQClTRw7mH6eK+KBiI7vq9nOg8TD395vLxmWVHK2w
f6OvxiYXm7aXkpWRhNvj4/cvvk+Dw4Wma8THRRMfF42UgpJDJ6l3uPii5DjNTg8VVXVIITlV29gi
ZtmRqrM1oPz4Kerqm6myN1BRdbq1bgi+PlKFvc6BYZotPgy/SVl5FaZSJMTH0OBwsvBvH1N+vOZC
QulCqCX1R7bVBny3IfuqR/YrxNALVd+IVNQPVIHF145QoHtBdwl0J9x3/SQi8+28XL4cXUgG9+rH
LOftPL9oHY5m97fy/X1BoWqR/ivLNywshe/oAKbmEeiuMwu1ZxAmaB6wOAURDoHuBuEHoeDtFdu4
1zqZeWNmcdxdyTXWAl5esuOSFakjvhOhhAosKRlRCs0rsDjB4hBY3CA7mAh5vQavLtvC1SdySYzP
5c2v9nHw6KlQx+ai4jtb5YysF1iaBZo30Jo6o9nl4cO1e0Mdj4uW70wo4QfdH+rqfX8Ib8X3EMJC
9RDCQvUQgoQykTvON6MwXY342uG1tizPB00m3Ia2IFL3CqFEdqiLeUkjOKaEeMle/N+OVlM7lEi+
cl74LmYIsbh1/8mdf3FeeE5hwoQJ06P5f4pcGmyOmGq/AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIy
LTA3LTAxVDE5OjIwOjUwKzAyOjAwZoUhGAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMi0wNy0wMVQx
OToyMDo1MCswMjowMBfYmaQAAAAASUVORK5CYII="
            />
        </svg>
    );
}
