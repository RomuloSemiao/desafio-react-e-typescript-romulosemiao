import React, { createContext, useContext, useState } from "react";

interface MenuContextProps {
    isOpen: boolean;
    setIsOpen: (state: boolean) => void;
}

const MenuContext = createContext({} as MenuContextProps);

export const MenuProvider = ({ children }: { children: React.ReactNode }) => {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <MenuContext.Provider
            value={{
                isOpen,
                setIsOpen,
            }}
        >
            {children}
        </MenuContext.Provider>
    );
};

export const useMenuContext = () => {
    return useContext(MenuContext);
};
