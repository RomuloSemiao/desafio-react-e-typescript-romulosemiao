import React from "react";

import { Routes, Route } from "react-router-dom";
import { Contact } from "../components/Contact/Contact";
import { Sobre } from "../components/Sobre/Sobre";

export function RoutesComponent() {
    return (
        <Routes>
            <Route path="/*" element={<Sobre />} />
            <Route path="/contact" element={<Contact />} />
        </Routes>
    );
}
